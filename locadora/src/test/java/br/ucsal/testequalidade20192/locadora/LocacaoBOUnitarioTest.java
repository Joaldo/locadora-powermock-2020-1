package br.ucsal.testequalidade20192.locadora;



import org.junit.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import static org.powermock.api.mockito.PowerMockito.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ucsal.testequalidade20192.locadora.business.*;
import br.ucsal.testequalidade20192.locadora.dominio.*;
import br.ucsal.testequalidade20192.locadora.persistence.*;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitPlatform.class)
@PrepareForTest({LocacaoBO.class, ClienteDAO.class,VeiculoDAO.class, LocacaoDAO.class})
public class LocacaoBOUnitarioTest {

	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		
		
		Cliente cliente = new Cliente("04986544879", "Jota", "45986250");
		Modelo modelo = new Modelo("Palio");
		Veiculo veiculo = new Veiculo("ABC-1234", 2009, modelo, 12.50);
		String placa = "ABC-1234";
		Date data = new Date();
		
		
		List<Veiculo> veiculos = new ArrayList<>();
		List<String> placas = new ArrayList<>();
		veiculos.add(veiculo);
		placas.add(placa);
		
		Locacao locacao = new Locacao(cliente, veiculos, data, 5);
		
		mockStatic(ClienteDAO.class);
		
		when(ClienteDAO.obterPorCpf("04986544879")).thenReturn(cliente);		
		mockStatic(VeiculoDAO.class);
		
		when(VeiculoDAO.obterPorPlaca("ABC-1234")).thenReturn(veiculo);		
		mockStatic(LocacaoDAO.class);
		
		whenNew(Locacao.class).withAnyArguments().thenReturn(locacao);
		LocacaoBO.locarVeiculos("ABC-1234", placas, data, 5);

		verifyStatic(ClienteDAO.class);
		ClienteDAO.obterPorCpf("04986544879");
		
		verifyStatic(VeiculoDAO.class);
		VeiculoDAO.obterPorPlaca("ABC-1234");
		
		verifyStatic(LocacaoDAO.class);
		LocacaoDAO.insert(locacao);
		
		verifyNoMoreInteractions(LocacaoBO.class);

	}
}